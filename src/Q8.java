import java.util.Random;
import java.util.Scanner;

public class Q8 {
    public static void main(String args[]) {
        Random ran = new Random();
        int ranone = ran.nextInt(+10) + 10;
        System.out.println("請輸入" + ranone + "個整數:");
        int[] a = new int[ranone];
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < ranone; i++) {
            a[i] = scanner.nextInt();
        }
        int temp;
        for (int j = 0; j < ranone; j++) {
            for (int i = 0; i < ranone - 1; i++) {
                if (a[i] > a[i + 1]) {
                    temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;
                }
            }
        }
        System.out.print("請輸入U或D:");
        char in;
        in = scanner.next().charAt(0);
        switch (in) {
            case 'U':
                for (int i = 0; i < ranone; i++) {
                    System.out.print(a[i] + " ");
                }
                break;
            case 'D':
                for (int i = ranone - 1; i >= 0; i--) {
                    System.out.print(a[i] + " ");
                }
                break;
        }
    }
}