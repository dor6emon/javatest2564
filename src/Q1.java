import sun.net.www.http.Hurryable;
import sun.net.www.protocol.http.HttpURLConnection;

import java.util.Scanner;

public class Q1 {
    public static void main(String[] args) {
        int num1, Hnum, Tnum, Anum, add, sub, mul;
        Scanner scanner = new Scanner(System.in);

        System.out.println("請輸入100-999");

        num1 = scanner.nextInt();
        Hnum = num1 / 100;
        Tnum = (num1 / 10) % 10;
        Anum = num1 % 10;

        add = Hnum + Tnum + Anum;
        sub = Hnum - Tnum - Anum;
        mul = Hnum * Tnum * Anum;

        System.out.println("每位數和 = " + add + " 每位數積 = " + mul + " 每位數差 = " + sub);

    }

}
