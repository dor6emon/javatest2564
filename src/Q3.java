import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Q3 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int num1, add = 0, sub = 0, sum = 0, avg = 0, all = 0;
        System.out.println("請輸入數字(0停止) : ");
        num1 = scanner.nextInt();

        while (num1 != 0) {
            if (num1 > 0) {
                add++;
                all++;
                sum += num1;
            } else if (num1 < 0) {
                sub++;
                all++;
                sum += num1;
            }
            num1 = scanner.nextInt();
        }
        System.out.println("正數 = " + add + " 負數 = " + sub + " 總數 = " + sum + " 平均 = " + (sum / all));

    }
}

